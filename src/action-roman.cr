require "action-controller"
require "roman"

class RomanController < AC::Base
  base "/convert"
  @[AC::Route::GET("/:number")]
  def convert(number : Int32)
    number.to_roman
  end
end
