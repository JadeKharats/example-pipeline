Feature: Testing Roman API 

  Scenario: Making a GET request to the API with variables to convert
    Given I have an API endpoint "http://localhost:3000/convert/{variable}"
    When I send a GET request with variable "123"
    Then the response status should be 200
    And the response body should contain "CXXIII"
