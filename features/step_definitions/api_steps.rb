require 'httparty'
require 'rspec'

Given(/^I have an API endpoint "(.*?)"$/) do |endpoint|
  @endpoint = endpoint
end

When(/^I send a GET request with variable "(.*?)"$/) do |variable_value|
  # Replace '{variable}' in the endpoint with the actual variable value
  url_with_variable = @endpoint.gsub('{variable}', variable_value)
  @response = HTTParty.get(url_with_variable)
end

Then(/^the response status should be (\d+)$/) do |status_code|
  expect(@response.code).to eq(status_code.to_i)
end

Then(/^the response body should contain "(.*?)"$/) do |expected_text|
  expect(@response.body).to include(expected_text)
end

