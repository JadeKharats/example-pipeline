import http from 'k6/http';
import { describe, expect } from 'https://jslib.k6.io/k6chaijs/4.3.4.3/index.js';
 
export default function convert() {
  const url = "http://localhost:3000/convert/123"

  const params = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  const response = http.get(url, params); 

  expect(response.status, 'response status').to.equal(200);
  expect(response).to.have.validJsonBody();
  expect(response.json(), 'root').to.be.an('string');
}
