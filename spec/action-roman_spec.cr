require "./spec_helper"

describe RomanController do
  roman = RomanController.spec_instance(HTTP::Request.new("GET", "/"))
  it "return I" do
    roman.convert(1).should eq "I"
  end

  client = AC::SpecHelper.client

  it "respond I via HTTP call" do
    result = client.get("/convert/1")
    result.body.should eq %("I")
  end
end
