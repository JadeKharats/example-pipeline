# action-roman

example of local CI and Gitlab-ci for a crystal-lang repository

## Installation

### Local CI

- FSevent : [guardian](https://github.com/f/guardian#installation)
- CI local : [werk](https://github.com/marghidanu/werk/wiki/Installation)
- Process Manager : [overmind](https://github.com/DarthSim/overmind#installation)
- `crystal spec`, `crystal tool format`, `crystal tool unreachable`, `crystal docs` are provided by the [Crystal-lang installation](https://crystal-lang.org/install/)
- Linter : [ameba](https://github.com/crystal-ameba/ameba#installation)
- Coverage : [crkcov](https://github.com/crystal-community/crystal-kcov) depends of [Kcov](https://github.com/SimonKagstrom/kcov/blob/master/INSTALL.md)
- Mutation testing : [crytic](https://github.com/hanneskaeufler/crytic#installation)
- Functionnal testing : [cucumber](https://github.com/cucumber/cucumber-ruby#installation)
- Performance testing : [k6](https://k6.io/docs/get-started/installation/)

## Usage

When working on the code, run `guardian -c`.
After each saving of `*.cr` files, the pipeline will be launched

You can start the pipeline manually with the `werk run` command

You can also run specific tests. For example `werk run perf:soak_test` will launch the pipeline to run soak tests


## Contributors

- [Jade D. Kharats](https://gitlab.com/JadeKharats) - creator and maintainer
